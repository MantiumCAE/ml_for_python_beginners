"""A few helper functions for the Python Beginners Machine Learning Training.
Most functions get developed by the students.
"""

import pandas as pd


old_team_names_2_new = {
    "Force India": "Aston Martin",
    "Lotus": "Lotus",
    "Manor": "Manor",
    "Ferrari": "Ferrari",
    "Force India (Racing Point)": "Aston Martin",
    "Renault": "Alpine",
    "Toro Rosso": "AlphaTauri",
    "AlphaTauri": "AlphaTauri",
    "Virgin": "Manor",
    "HRT": "HRT",
    "Mercedes": "Mercedes",
    "Haas F1": "Haas F1",
    "Williams": "Williams",
    "Caterham": "Caterham",
    "Sauber": "Alfa Romeo",
    "McLaren": "McLaren",
    "Racing Point": "Aston Martin",
    "Red Bull": "Red Bull",
    "Alfa Romeo": "Alfa Romeo",
    "Manor-Marussia": "Manor",
    "Aston Martin": "Aston Martin",
    "Alpine": "Alpine",
}


def convert_time_str_to_int(input_str):
    """This function takes a string input and converts it to an integer.
    Sometimes the input is not a valid time, so None gets returned.
    """
    if not type(input_str) is str:
        time = None
    elif (":" in input_str) and ("." in input_str) and not ("--" in input_str):
        minutes, rest = input_str.split(":")
        seconds, msecs = rest.split(".")
        time = int(minutes) * 60 * 1000 + int(seconds) * 1000 + int(msecs)
    else:
        time = None
    return time


def calc_mean(input_list):
    tot_sum = 0
    n_vals = 0
    for time in input_list:
        int_time = convert_time_str_to_int(time)
        if type(int_time) is int:
            tot_sum += int_time
            n_vals += 1
    return tot_sum / n_vals


def calc_std(input_list):
    mean = calc_mean(input_list)
    sums, n_vals = 0, 0
    for time in input_list:
        int_time = convert_time_str_to_int(time)
        if type(int_time) is int:
            sums += (int_time - mean) ** 2
            n_vals += 1
    return (sums / n_vals) ** 0.5


def adjust_team_name(input_name_str):
    #    print(f'Converting: {input_name_str}')
    if input_name_str in old_team_names_2_new:
        new_name = old_team_names_2_new[input_name_str]
    else:
        print(f"No conversion found for: {input_name_str}")
        new_name = input_name_str
    #    print(f'to: {new_name}')
    return new_name


pos2pts = {1: 25, 2: 18, 3: 15, 4: 12, 5: 10, 6: 8, 7: 6, 8: 4, 9: 2, 10: 1}


def calc_champ_points(pos):
    if pos in pos2pts:
        points = pos2pts[pos]
    else:
        points = 0
    return points


def calc_historic_vals(race_df, cols_4_hist, window_length=6):
    """Small helper function that takes a df and a list of columns
    to calculate gliding averages on. The default window length is 5.
    Returns the df with new columns
    containing the historic data.
    """
    print("About to calculate historic gliding averages, this might take a while...")
    hist_dfs = []

    for driver in race_df["Driver"].unique():
        _hist_df = race_df[race_df["Driver"] == driver].reset_index(drop=True)

        # _hist_df["r_pos"] = _hist_df["r_pos"].fillna(20)

        for hcol in cols_4_hist:
            _hist_df[f"hist_{hcol}"] = (
                _hist_df[hcol].shift(1).rolling(window_length, min_periods=1).mean()
            )
        hist_dfs.append(_hist_df)
    hist_df = pd.concat(hist_dfs)
    hist_cols = [col for col in hist_df.columns if col.startswith("hist_")] + [
        "Driver",
        "race_idx",
    ]
    hist_df = hist_df[hist_cols].reset_index(drop=True)

    race_df = race_df.merge(
        hist_df, left_on=["Driver", "race_idx"], right_on=["Driver", "race_idx"]
    )

    print("Done with gliding average calculations.")
    return race_df.sort_values(by=["race_idx", "race_pos"], ignore_index=True)
